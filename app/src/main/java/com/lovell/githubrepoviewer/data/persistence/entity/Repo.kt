package com.lovell.githubrepoviewer.data.persistence.entity

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json

@Entity(tableName = "repos")
data class Repo(
        @PrimaryKey
        @Json(name = "id")
        val id: Long,
        @Json(name = "name")
        val name: String,
        @Json(name = "owner")
        @Embedded(prefix = "owner_")
        val owner: Owner,
        @Json(name = "description")
        val description: String? = null,
        @Json(name = "url")
        val url: String,
        @Json(name = "created_at")
        val createdAt: String?,
        @Json(name = "updated_at")
        val updatedAt: String?,
        @Json(name = "license")
        @Embedded(prefix = "license_")
        val license: License?,
        @Json(name = "forks")
        val forks: Long,
        @Json(name = "open_issues")
        val openIssues: Long,
        @Json(name = "watchers")
        val watchers: Long,
        @Json(name = "default_branch")
        val defaultBranch: String?
) {
        override fun toString(): String {
                return "id " + id + "\n" +
                       "name " +  name +  "\n" +
                       "owner " + owner + "\n" +
                       "description " + description + "\n" +
                       "url " + url + "\n" +
                       "createdAt " + createdAt +  "\n" +
                       "updatedAt " + updatedAt + "\n" +
                       "license " + license + "\n" +
                       "forks " + forks + "\n" +
                       "openIssues " + openIssues + "\n" +
                       "watchers " + watchers + "\n" +
                       "defaultBranch "+ defaultBranch + "\n"
        }
}