package com.lovell.githubrepoviewer.data

import android.util.Log
import com.lovell.githubrepoviewer.data.persistence.dao.LicenseDao
import com.lovell.githubrepoviewer.data.persistence.dao.OwnerDao
import com.lovell.githubrepoviewer.data.persistence.dao.RepoDao
import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import com.lovell.githubrepoviewer.data.remote.UserRepoService
import io.reactivex.Flowable
import kotlinx.coroutines.experimental.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository(private val userRepoService: UserRepoService,
                     private val licenseDao: LicenseDao,
                     private val repoDao: RepoDao,
                     private val ownerDao: OwnerDao) {

    fun getRepos(user: String): Flowable<List<Repo>> {
        getReposFromApi(user)
        return getReposFromDb(user)
    }


    private fun getReposFromApi(user: String) {
        for (page in 1..GOOGLE_MAX_PAGE_NUMBER) {
            userRepoService.getReposForUserCall(user, page)
                    .enqueue(object: Callback<List<Repo>> {
                        override fun onFailure(call: Call<List<Repo>>, t: Throwable) {}

                        override fun onResponse(call: Call<List<Repo>>,
                                                response: Response<List<Repo>>) {
                            if (response.isSuccessful) {
                                for (item in response.body() ?: ArrayList()) {
                                    insertRepo(item)
                                }
                            }
                        }

                    })
        }
    }

    private fun getReposFromDb(user: String): Flowable<List<Repo>> {
        return repoDao.getRepos()
    }

    fun insertRepo(repo: Repo) {
        launch {
            repoDao.insertRepo(repo)
        }
    }

    companion object {
        private const val GOOGLE_MAX_PAGE_NUMBER = 10
    }
}