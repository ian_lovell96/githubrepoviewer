package com.lovell.githubrepoviewer.data.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lovell.githubrepoviewer.data.persistence.entity.License
import io.reactivex.Single

@Dao
interface LicenseDao {

    @Query("SELECT * FROM licenses")
    fun queryRepos(): Single<List<License>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepo(license: License)
}