package com.lovell.githubrepoviewer.data.remote

import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface UserRepoService {
    @Headers("User-Agent: Github-Repo-Viewer",
            "Accept: application/vnd.github.v3+json")
    @GET("users/{user}/repos")
    fun getReposForUser(@Path("user") user: String): Observable<List<Repo>>


    @Headers("User-Agent: Github-Repo-Viewer",
            "Accept: application/vnd.github.v3+json")
    @GET("users/{user}/repos")
    fun getReposForUserCall(@Path("user") user: String, @Query("page") pageNumber: Int): Call<List<Repo>>
}