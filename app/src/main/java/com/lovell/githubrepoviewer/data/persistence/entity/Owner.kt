package com.lovell.githubrepoviewer.data.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


@Entity(tableName = "owners")
data class Owner(
        @Json(name = "login")
        val login: String,
        @PrimaryKey
        @Json(name = "id")
        val id: Long,
        @Json(name = "avatar_url")
        val avatarUrl: String?,
        @Json(name = "url")
        val url: String,
        @Json(name = "type")
        val type: String,
        @Json(name = "site_admin")
        val siteAdmin: Boolean)
