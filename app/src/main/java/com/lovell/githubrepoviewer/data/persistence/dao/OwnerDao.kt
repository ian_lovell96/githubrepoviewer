package com.lovell.githubrepoviewer.data.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lovell.githubrepoviewer.data.persistence.entity.Owner
import io.reactivex.Single

@Dao
interface OwnerDao {

    @Query("SELECT * FROM owners")
    fun queryRepos(): Single<List<Owner>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepo(owner: Owner)
}