package com.lovell.githubrepoviewer.data.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.lovell.githubrepoviewer.data.persistence.dao.LicenseDao
import com.lovell.githubrepoviewer.data.persistence.dao.OwnerDao
import com.lovell.githubrepoviewer.data.persistence.dao.RepoDao
import com.lovell.githubrepoviewer.data.persistence.entity.License
import com.lovell.githubrepoviewer.data.persistence.entity.Owner
import com.lovell.githubrepoviewer.data.persistence.entity.Repo

@Database(entities = [Repo::class, License::class, Owner::class], version = 1, exportSchema = false)
abstract class Database: RoomDatabase() {
    abstract fun repoDao(): RepoDao
    abstract fun licenseDao(): LicenseDao
    abstract fun ownerDao(): OwnerDao
}