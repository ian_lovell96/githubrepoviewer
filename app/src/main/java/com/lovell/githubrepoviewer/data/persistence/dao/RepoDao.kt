package com.lovell.githubrepoviewer.data.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import io.reactivex.Flowable

@Dao
interface RepoDao {

    @Query("SELECT * FROM repos ORDER BY name ASC")
    fun getRepos(): Flowable<List<Repo>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepo(repo: Repo)
}