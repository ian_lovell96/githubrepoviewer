package com.lovell.githubrepoviewer.data.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.Json


@Entity(tableName = "licenses")
data class License(
        @PrimaryKey
        @Json(name = "key")
        val key: String,
        @Json(name = "name")
        val name: String?,
        @Json(name = "spdx_id")
        val spdxId: String?,
        @Json(name = "url")
        val url: String?,
        @Json(name = "node_id")
        val nodeId: String?)