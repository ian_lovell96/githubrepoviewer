package com.lovell.githubrepoviewer


import android.app.Application
import android.content.Context
import androidx.room.Room
import com.lovell.githubrepoviewer.data.DataRepository
import com.lovell.githubrepoviewer.data.persistence.Database
import com.lovell.githubrepoviewer.data.persistence.dao.LicenseDao
import com.lovell.githubrepoviewer.data.persistence.dao.OwnerDao
import com.lovell.githubrepoviewer.data.persistence.dao.RepoDao
import com.lovell.githubrepoviewer.data.remote.UserRepoService
import com.lovell.githubrepoviewer.viewmodel.HomeFragmentViewModel
import com.lovell.githubrepoviewer.viewmodel.RepoDetailViewModel
import com.lovell.githubrepoviewer.viewmodel.RepoListViewModel
import com.lovell.githubrepoviewer.viewmodel.ViewModelFactory
import com.squareup.moshi.KotlinJsonAdapterFactory
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory

class RepoViewerApplication: Application() {
    companion object {
        val retrofit: Retrofit by lazy {
            Retrofit.Builder().baseUrl("https://api.github.com")
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder().add(
                            KotlinJsonAdapterFactory()).build())).build()
        }
        val userRepoService: UserRepoService by lazy { retrofit.create(UserRepoService::class.java) }
        private lateinit var database: Database
        private lateinit var repoDao: RepoDao
        private lateinit var licenseDao: LicenseDao
        private lateinit var ownerDao: OwnerDao
        lateinit var repository: DataRepository
        lateinit var viewModelFactory: ViewModelFactory
        lateinit var appContext: Context
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        database = Room.databaseBuilder(this, Database::class.java, "repo database")
                .fallbackToDestructiveMigration().build()
        repoDao = database.repoDao()
        licenseDao = database.licenseDao()
        ownerDao = database.ownerDao()
        repository = DataRepository(userRepoService, licenseDao, repoDao, ownerDao)
        viewModelFactory = ViewModelFactory(HomeFragmentViewModel(repository),
                RepoDetailViewModel(repository),
                RepoListViewModel(repository))
    }

}