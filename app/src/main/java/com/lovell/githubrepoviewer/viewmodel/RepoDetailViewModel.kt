package com.lovell.githubrepoviewer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lovell.githubrepoviewer.data.DataRepository
import com.lovell.githubrepoviewer.data.persistence.entity.Repo

class RepoDetailViewModel(private val dataRepository: DataRepository): ViewModel() {
    private val repo = MutableLiveData<Repo>()

    fun selectedRepo(): LiveData<Repo> {
        return repo
    }

    fun loadRepo(selectedRepo: Repo) {
        repo.postValue(selectedRepo)
    }
}