package com.lovell.githubrepoviewer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lovell.githubrepoviewer.data.DataRepository
import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import com.lovell.githubrepoviewer.navigation.Navigation
import com.lovell.githubrepoviewer.navigation.NavigationTarget
import com.lovell.githubrepoviewer.navigation.SelectedRepo

class RepoListViewModel(private val dataRepository: DataRepository): ViewModel() {
    private val repos = MutableLiveData<List<Repo>>()

    fun repos(): LiveData<List<Repo>> {
        return repos
    }

    fun loadRepos() {
        dataRepository.getRepos("google").subscribe { repos.postValue(it) }
    }

    fun repoSelected(repo: Repo) {
        SelectedRepo.selectedRepo.postValue(repo)
        Navigation.navigationTarget.postValue(NavigationTarget.REPO_DETAIL)
    }
}