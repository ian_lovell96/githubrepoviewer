package com.lovell.githubrepoviewer.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class ViewModelFactory(private val homeFragmentViewModel: HomeFragmentViewModel,
                       private val repoDetailViewModel: RepoDetailViewModel,
                       private val repoListViewModel: RepoListViewModel): ViewModelProvider.Factory {
    override fun <T: ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(HomeFragmentViewModel::class.java) -> homeFragmentViewModel as T
            modelClass.isAssignableFrom(RepoDetailViewModel::class.java)   -> repoDetailViewModel as T
            modelClass.isAssignableFrom(RepoListViewModel::class.java)     -> repoListViewModel as T
            else                                                           -> throw IllegalArgumentException(
                    "Unknown Class Name")

        }
    }

}