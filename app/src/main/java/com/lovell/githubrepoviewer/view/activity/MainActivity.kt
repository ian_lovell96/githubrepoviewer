package com.lovell.githubrepoviewer.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.lovell.githubrepoviewer.R
import com.lovell.githubrepoviewer.navigation.Navigation
import com.lovell.githubrepoviewer.navigation.NavigationTarget
import com.lovell.githubrepoviewer.view.fragment.HomeFragment
import com.lovell.githubrepoviewer.view.fragment.RepoDetailFragment
import com.lovell.githubrepoviewer.view.fragment.RepoListFragment

class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Navigation.navigationTarget.postValue(NavigationTarget.HOME)
        Navigation.navigationTarget.observe(this, Observer {
            when (it) {
                NavigationTarget.HOME        -> navigateToHomeFragment()
                NavigationTarget.REPO_DETAIL -> navigateToRepoDetailFragment()
                NavigationTarget.REPO_LIST   -> navigateToRepoListFragment()
                else                         -> navigateToHomeFragment()
            }
        })


    }

    private fun navigateToHomeFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.content, HomeFragment()).commit()
    }

    private fun navigateToRepoListFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.content, RepoListFragment()).commit()
    }

    private fun navigateToRepoDetailFragment() {
        supportFragmentManager.beginTransaction().replace(R.id.content, RepoDetailFragment())
                .commit()
    }
}
