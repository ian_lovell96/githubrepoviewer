package com.lovell.githubrepoviewer.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.lovell.githubrepoviewer.R
import com.lovell.githubrepoviewer.RepoViewerApplication
import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import com.lovell.githubrepoviewer.navigation.Navigation
import com.lovell.githubrepoviewer.navigation.NavigationTarget
import com.lovell.githubrepoviewer.navigation.SelectedRepo
import com.lovell.githubrepoviewer.viewmodel.RepoDetailViewModel
import kotlinx.android.synthetic.main.fragment_repo_detail.*

class RepoDetailFragment: Fragment() {
    private lateinit var repoListViewModel: RepoDetailViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repo_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        repoListViewModel =
                ViewModelProviders.of(this, RepoViewerApplication.viewModelFactory)
                        .get(RepoDetailViewModel::class.java)

        SelectedRepo.selectedRepo.observe(this,
                Observer<Repo> { t ->
                    t?.let {
                        repoListViewModel.loadRepo(it)
                    }
                })

        repoListViewModel.selectedRepo().observe(this,
                Observer<Repo> { t ->
                    t?.let {
                        repo_name.text = it.name
                        repo_description.text = it.description ?: getString(R.string.repo_description_none)
                        repo_created.text = it.createdAt ?: ""
                        repo_updated.text = it.updatedAt ?: ""
                        repo_link.text = it.url
                        repo_license.text = it.license?.name ?: getString(R.string.repo_license_none)
                    }
                })

        back_home.setOnClickListener {
            Navigation.navigationTarget.postValue(NavigationTarget.HOME)
        }
    }
}