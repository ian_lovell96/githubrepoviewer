package com.lovell.githubrepoviewer.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.lovell.githubrepoviewer.R
import com.lovell.githubrepoviewer.RepoViewerApplication
import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import com.lovell.githubrepoviewer.navigation.SelectedRepo
import com.lovell.githubrepoviewer.util.SelectedCallback
import com.lovell.githubrepoviewer.view.adapter.RepoListAdapter
import com.lovell.githubrepoviewer.viewmodel.RepoListViewModel
import kotlinx.android.synthetic.main.fragment_repo_list.*

class RepoListFragment: Fragment() {
    private lateinit var repoListViewModel: RepoListViewModel
    private lateinit var linearLayoutManager: LinearLayoutManager
    private lateinit var listAdapter: RepoListAdapter

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_repo_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        repoListViewModel =
                ViewModelProviders.of(this, RepoViewerApplication.viewModelFactory)
                        .get(RepoListViewModel::class.java)
        repoListViewModel.loadRepos()

        linearLayoutManager = LinearLayoutManager(activity)
        repo_list.layoutManager = linearLayoutManager

        listAdapter = RepoListAdapter(ArrayList())
        listAdapter.addOnClickListener(object: SelectedCallback<Repo?> {
            override fun onSelection(selection: Repo?) {
                selection?.let {
                    repoListViewModel.repoSelected(it)
                }
            }

        })
        repo_list.adapter = listAdapter

        repoListViewModel.repos().observe(this, Observer<List<Repo>> {
            it?.let { repoList ->
                listAdapter.repos = if (repoList.isNotEmpty()) {
                    repoList
                } else {
                    ArrayList()
                }
                repo_list.adapter = listAdapter

            }
        })

    }
}