package com.lovell.githubrepoviewer.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.lovell.githubrepoviewer.R
import com.lovell.githubrepoviewer.navigation.Navigation
import com.lovell.githubrepoviewer.navigation.NavigationTarget
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        to_list.setOnClickListener {
            Navigation.navigationTarget.postValue(NavigationTarget.REPO_LIST)
        }


    }
}