package com.lovell.githubrepoviewer.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.lovell.githubrepoviewer.R
import com.lovell.githubrepoviewer.data.persistence.entity.Repo
import com.lovell.githubrepoviewer.util.SelectedCallback
import kotlinx.android.synthetic.main.repo_view.view.*

class RepoListAdapter(var repos: List<Repo>): RecyclerView.Adapter<RepoListAdapter.RepoHolder>() {
    private var onClickListener: SelectedCallback<Repo?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoHolder {
        return RepoHolder(parent.inflate(R.layout.repo_view, false))
    }

    override fun getItemCount(): Int {
        return repos.size
    }

    override fun onBindViewHolder(holder: RepoHolder, position: Int) {
        holder.bindRepo(repos[position])
        onClickListener?.let { holder.setOnClickListener(it) }
    }

    fun addOnClickListener(listener: SelectedCallback<Repo?>) {
        this.onClickListener = listener
    }


    class RepoHolder(v: View): RecyclerView.ViewHolder(v) {
        private var view = v
        private var repo: Repo? = null
        private var onClickListener: SelectedCallback<Repo?>? = null


        fun bindRepo(repo: Repo) {
            this.repo = repo
            view.setOnClickListener { _ ->
                onClickListener?.onSelection(this.repo)
            }
            view.list_repo_name.setOnClickListener {
                onClickListener?.onSelection(this.repo)
            }
            view.list_repo_name.text = repo.name
            view.list_repo_owner.text = repo.owner.login
            view.list_repo_updated.text = repo.updatedAt
        }

        fun setOnClickListener(onClickListener: SelectedCallback<Repo?>) {
            this.onClickListener = onClickListener
        }
    }
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}