package com.lovell.githubrepoviewer.navigation

import androidx.lifecycle.MutableLiveData

class Navigation {
    companion object {
        @JvmStatic
        val navigationTarget = MutableLiveData<NavigationTarget>()
    }
}

enum class NavigationTarget {
    HOME, REPO_DETAIL, REPO_LIST
}