package com.lovell.githubrepoviewer.navigation

import androidx.lifecycle.MutableLiveData
import com.lovell.githubrepoviewer.data.persistence.entity.Repo

class SelectedRepo {
    companion object {
        @JvmStatic
        val selectedRepo = MutableLiveData<Repo>()
    }
}