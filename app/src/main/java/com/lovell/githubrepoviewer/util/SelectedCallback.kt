package com.lovell.githubrepoviewer.util

interface SelectedCallback<T> {
    fun onSelection(selection: T)
}